package com.maple.jan;

import com.google.common.eventbus.Subscribe;
import com.maple.quickstart.OrderEvent;

/**
 * @Desc:
 * @author: maple
 * @Date: 2018-02-07 17:55
 */
public class MultiEventListener {

    @Subscribe
    public void onEvent(OrderEvent orderEvent) {
        System.out.println("MultiEventListener: receive msg: "+orderEvent.getMessage());
    }

    @Subscribe
    public void onEvent(String orderEvent) {
        System.out.println("MultiEventListener: receive String msg: "+orderEvent);
    }

}
