package com.maple.quickstart;

import com.google.common.eventbus.EventBus;
import com.maple.jan.MultiEventListener;

/**
 * Desc:
 * author: maple
 * Date: 2018-02-07 17:50
 */
public class SimpleEventBus {

    public static void main(String[] args) {
        EventBus eventBus = new EventBus("maple");

        eventBus.register(new OrderEventListener());
        eventBus.register(new MultiEventListener());

        eventBus.post(new OrderEvent("hello"));
        eventBus.post(new OrderEvent("maple"));

        eventBus.post("string");
        eventBus.post(1);

    }
}
