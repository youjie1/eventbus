package com.maple.quickstart;

/**
 * Desc:
 * author: maple
 * Date: 2018-02-07 17:48
 */
public class OrderEvent {

    private String message;

    public OrderEvent(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
