package com.maple.quickstart;

import com.google.common.eventbus.DeadEvent;
import com.google.common.eventbus.Subscribe;

/**
 * @Desc:
 * @author: maple
 * @Date: 2018-02-07 17:49
 */
public class OrderEventListener {

    @Subscribe
    public void onEvent(OrderEvent orderEvent) {
        System.out.println("OrderEventListener: receive msg: "+orderEvent.getMessage());
    }

    @Subscribe
    public void onEvent(DeadEvent deadEvent) {
        System.out.println("deadEvent  "+ deadEvent);
    }

}
